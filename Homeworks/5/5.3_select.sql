-- 1. Print number of teachers in the Cybersecurity department. 
SELECT COUNT(DISTINCT t.Id) AS 'Number of Teachers'
FROM Teachers AS t, Lectures AS l, GroupsLectures AS gl, Groups AS g, Departments AS d
WHERE d.Name = 'Cybersecurity' AND d.Id = g.DepartmentId AND g.Id = gl.GroupId AND gl.LectureId = l.Id AND l.TeacherId = t.Id

-- 2. Print number of lectures that the teacher "Arthur Wiliams" delivers. 
SELECT COUNT(l.Id) AS 'Number of Lectures'
FROM Lectures AS l, Teachers AS t
WHERE t.Name = 'Arthur' AND t.Surname = 'Wiliams' AND t.Id = l.TeacherId

-- 3. Print number of classes given in the classroom "A312".
SELECT COUNT(l.Id) AS 'Number of Classes'
FROM Lectures AS l
WHERE l.LectureRoom = 'A312'

-- 4. Print names of the classrooms and number of lectures delivered in them. 
SELECT LectureRoom AS 'Name of the Classroom', COUNT(SubjectId) AS 'Number of Lectures'
FROM Lectures
GROUP BY LectureRoom

-- 5. Print number of students attending lectures of the teacher "Jamie Brown".
SELECT COUNT(s.Id) AS 'Number of Students'
FROM Students AS s, GroupsLectures AS gl, Lectures AS l, Teachers AS t
WHERE s.GroupId = gl.GroupId AND gl.LectureId = l.Id AND l.TeacherId = t.Id AND t.Name = 'Jamie' AND t.Surname = 'Brown'

-- 6. Print average wage rate of teachers of the Computer Science faculty.
SELECT AVG(t.Salary)
FROM Teachers AS t, Lectures AS l, GroupsLectures AS gl, Groups AS g, Departments AS d, Faculties AS f
WHERE t.Id = l.TeacherId AND l.Id = gl.LectureId AND gl.GroupId = g.Id AND g.DepartmentId = d.Id AND d.FacultyId = f.Id AND f.Name = 'Computer Science'

-- 7. Print minimum and maximum number of students among all groups.
SELECT MAX(n.NumStudents) AS 'Max number of students', MIN(n.NumStudents) AS 'Min number of students'
FROM (
SELECT g.Id, COUNT(s.Id) AS NumStudents
FROM Students AS s, Groups AS g
WHERE s.GroupId = g.Id
GROUP BY g.Id
) n

-- 8. Print average financing fund of departments. 
SELECT AVG(Financing) AS 'Average fin fund'
FROM Departments

-- 9. Print full names of teachers and number of subjects they teach. 
SELECT t.Name + ' ' + t.Surname AS 'Full name', COUNT(s.Id) AS 'Number of subjects'
FROM Teachers AS t, Lectures AS l, Subjects AS s
WHERE s.Id = l.SubjectId AND l.TeacherId = t.Id
GROUP BY t.Name + ' ' + t.Surname


-- 10. Print number of lectures on each day of the week.
SELECT gl.DayOfWeek AS 'Day of week', COUNT(l.Id) AS 'Number of Lectures'
FROM GroupsLectures AS gl, Lectures AS l
WHERE gl.LectureId = l.Id
GROUP BY gl.DayOfWeek

-- 11. Print numbers of classrooms and number of departments whose lectures are delivered in them.
SELECT l.LectureRoom AS 'Classroom', d.Id AS 'Department'
FROM Lectures AS l, GroupsLectures AS gl, Groups AS g, Departments AS d
WHERE l.Id = gl.LectureId AND gl.GroupId = g.Id AND g.DepartmentId = d.Id

-- 12. Print names of the faculties and number of subjects that are taught in them.
SELECT f.Name, COUNT(S.Id) AS 'Number of Subjects'
FROM Faculties AS f, Departments AS d, Groups AS g, GroupsLectures AS gl, Lectures AS l, Subjects AS  s
WHERE f.Id = d.FacultyId AND d.Id = g.DepartmentId AND g.Id = gl.GroupId AND gl.LectureId = l.Id AND l.SubjectId = s.Id
GROUP BY f.Name

-- 13.  Print number of lectures for each teacher-classroom pair.
SELECT t.Name + ' ' + t.Surname AS 'Teacher name', l.LectureRoom, COUNT(l.Id) AS 'Number of lectures'
FROM Lectures AS l, Teachers AS t
WHERE l.TeacherId = t.Id
GROUP BY t.Name + ' ' + t.Surname, l.LectureRoom