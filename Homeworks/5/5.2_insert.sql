INSERT INTO Teachers(Name, Surname, Salary)
VALUES ('Amy', 'Black', 1050),
('Arthur', 'Wiliams', 950),
('Jamie', 'Brown', 1200);

INSERT INTO Subjects(Name)
VALUES ('Financial markets'),
('Investments'),
('Network Security'),
('English literature');

INSERT INTO Lectures(DayOfWeek, LectureRoom, SubjectId, TeacherId)
VALUES (3, 'A312', 1, 2),
(4, 'P102', 2, 2),
(1, 'A301', 3, 3),
(2, 'P230', 4, 1);

INSERT INTO Faculties (Name)
VALUES ('Economics'),
('Computer Science'),
('Arts');

INSERT INTO Departments(Name, Financing, FacultyId)
VALUES ('Finance', 32000, 1),
('Cybersecurity', 40000, 2),
('Linguistics', 29000, 3);

INSERT INTO Groups(Name, Year, DepartmentId)
VALUES ('E120', 3, 1),
('C108', 5, 2),
('A241', 4, 3);

INSERT INTO GroupsLectures (DayOfWeek, GroupId, LectureId)
VALUES (4, 1, 1),
(3, 1, 2),
(3, 2, 3),
(1, 3, 4);

INSERT INTO Students(Name, Surname, GroupId)
VALUES ('Jake', 'Cordon', 1),
('Jacob', 'Bass', 2),
('Karen', 'Clark', 3);

INSERT INTO Students(Name, Surname, GroupId)
VALUES ('John', 'Gates', 1),
('Carol', 'Smith', 2);


INSERT INTO Students(Name, Surname, GroupId)
VALUES ('Oliver', 'Stark', 1);