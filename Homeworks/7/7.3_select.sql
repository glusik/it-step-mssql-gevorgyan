-- 1. Print names of the classrooms where the teacher "Harry Miller" lectures.

SELECT lr.Name
FROM LectureRooms AS lr
WHERE EXISTS (
	SELECT *
	FROM 
		Schedules AS sch,
		Lectures AS l,
		Teachers AS t
	WHERE lr.Id = sch.LectureRoomId 
	AND sch.LectureId = l.Id 
	AND l.TeacherID = t.Id 
	AND t.Name = 'Harry' 
	AND t.Surname = 'Miller' )

-- 2. Print names of the assistants who deliver lectures for the group "A241".
SELECT t.Name + ' ' + t.Surname AS 'Assistant name'
FROM Teachers AS t
WHERE EXISTS (
	SELECT *
	FROM 
		Assistants AS a,
		Lectures AS l, 
		GroupsLectures AS gl, 
		Groups AS g
	WHERE a.TeacherID = t.Id 
	AND t.Id = l.TeacherID 
	AND l.Id = gl.LectureId 
	AND gl.GroupId = g.Id 
	AND g.Name = 'A241'
)

-- 3. Print subjects taught by the teacher "Lilian Wilson" for groups of the 5th year.
SELECT s.Name
FROM Subjects AS s
WHERE EXISTS (
	SELECT*
	FROM 
		Lectures AS l, 
		GroupsLectures AS gl, 
		Groups AS g, 
		Teachers AS t
	WHERE 
		s.Id = l.SubjectId 
	AND	l.Id = gl.LectureId 
	AND gl.GroupId = g.Id 
	AND g.Year = 3 
	AND t.Name = 'Lilian' 
	AND t.Surname = 'Wilson' 
	AND t.Id = l.TeacherID
)

-- 4. Print names of the teachers who do not deliver lectures on Mondays.
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers AS t
WHERE t.Id = ANY (
	SELECT l.TeacherID
	FROM
		Lectures AS l,
		Schedules AS s
	WHERE l.Id = s.LectureId
	AND	S.DayOfWeek <> 1
)

-- 5. Print names of the classrooms, indicating their buildings, in which there are no lectures on Wednesday of the second week on the third double period.
SELECT lr.Name, lr.Building
FROM LectureRooms AS lr
WHERE lr.Id = ANY (
	SELECT s.LectureRoomId
	FROM Schedules AS s
	WHERE s.DayOfWeek <> 3
	OR s.Week <> 2
)

-- 6. Print full names of teachers of the Computer Science faculty, who do not supervise groups of the Cybersecurity department.
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers AS t
WHERE t.Id = ANY (
	SELECT c.TeacherId
	FROM 
		Faculties AS f,
		Departments AS d,
		Groups AS g,
		GroupsCurators AS gc,
		Curators AS c
	WHERE f.Name = 'Computer Science'
	AND f.Id = d.FacultyId
	AND d.Name <> 'Cybersecurity'
	AND d.Id = g.DepartmentId
	AND g.Id = gc.GroupId
	AND gc.CuratorId = c.Id)

-- 7.Print numbers of all buildings that are available in the tables of faculties, departments, and classrooms.
SELECT f.Building
FROM Faculties AS f
UNION
SELECT d.Building
FROM Departments AS d
UNION
SELECT lr.Building
FROM LectureRooms AS lr

-- 8. Print full names of teachers in the following order: deans of faculties, heads of departments, teachers, curators, assistants.
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers t
JOIN Deans d
ON d.TeacherID = t.Id
UNION
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers t
JOIN Heads h
ON h.TeacherId = t.Id
UNION
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers t
UNION
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers t
JOIN Curators c
ON c.TeacherId = t.Id
UNION
SELECT t.Name + ' ' + t.Surname AS 'Teachers name'
FROM Teachers t
JOIN Assistants a
ON a.TeacherID = t.Id

-- 9. Print days of the week (without repetitions), in which there are classes in the classrooms "A301" and "A104" of the building 1.
SELECT s.DayOfWeek
FROM Schedules s
LEFT JOIN LectureRooms lr
ON s.LectureRoomId = lr.Id
WHERE lr.Building = 1
AND lr.Name = 'A301'
OR lr.Name = 'A104'