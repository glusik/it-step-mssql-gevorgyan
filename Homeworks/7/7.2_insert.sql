INSERT INTO LectureRooms (Name, Building)
VALUES ('A301',1), ('A134',3), ('B110',4), ('B201',5);

INSERT INTO Subjects (Name)
VALUES ('Financial markets'), ('Investments'), ('Network Security'), ('English literature');

INSERT INTO Teachers(Name, Surname)
VALUES ('Harry', 'Miller'), ('Lilian', 'Wilson'), ('Gracie', 'Evans'), ('Jackie', 'Jones');

INSERT INTO Assistants(TeacherId)
VALUES (1), (2), (3), (4);

INSERT INTO Lectures(SubjectId, TeacherId)
VALUES (1, 1), (2,2), (3,3), (4,4);

INSERT INTO Schedules (Class, DayOfWeek, Week, LectureId, LectureRoomId)
VALUES (2, 1, 12, 1, 1), (4, 3, 19, 2, 2), (1, 5, 23, 3, 3), (4, 4, 35, 4, 4);

INSERT INTO Deans(TeacherId)
VALUES (1), (3), (4);

INSERT INTO Faculties(Name, Building, DeanId)
VALUES ('Economics', 1, 1), ('Computer Science', 2, 2), ('Arts', 3, 3);

INSERT INTO Heads(TeacherId)
VALUES (2), (3), (4);

INSERT INTO Departments(Name, Building, FacultyId, HeadId)
VALUES ('Finance', 1, 1, 1), ('Cybersecurity', 2, 2, 2), ('Linguistics', 3, 3, 3);

INSERT INTO Curators(TeacherId)
VALUES (2), (3), (4);

INSERT INTO Groups(Name, Year, DepartmentId)
VALUES ('E120', 3, 1), ('C108', 5, 2), ('A241', 4, 3);

INSERT INTO GroupsCurators(CuratorId, GroupId)
VALUES (1, 1), (2, 2), (3, 3);

INSERT INTO GroupsLectures(GroupId, LectureId)
VALUES (1, 1), (1, 2), (2, 3), (3, 4);





