-- 1. Print all possible pairs of lines of teachers and groups. 
SELECT t.Name + ' ' + t.Surname AS TeachersName, g.Name
FROM Teachers AS t, Groups AS g, Lectures AS l, GroupsLectures AS gl
WHERE t.Id = l.TeacherId AND l.Id = gl.LectureId AND gl.GroupId = g.Id

-- 2. Print names of faculties, where financing fund of departments exceeds financing fund of the faculty. 
SELECT f.Name
FROM Faculties AS f, Departments AS d
WHERE d.Financing > f.Financing

-- 3. Print names of the group curators and groups names they are supervising. 
SELECT c.Name, g.Name
FROM Curators AS c, Groups AS g, GroupsCurators AS gc
WHERE c.Id = gc.CuratorId AND g.Id = gc.GroupId

-- 4. Print names of the teachers who deliver lectures in the group "E120".
SELECT DISTINCT t.Name
FROM Teachers AS t, Groups AS g, GroupsLectures AS gl, Lectures AS l
WHERE t.Id = l.TeacherId AND l.Id = gl.LectureId AND g.Id = gl.GroupId AND g.Name = 'E120'

-- 5. Print names of the teachers and names of the faculties where they are lecturing.
SELECT DISTINCT t.Name AS TeacherName, f.Name AS FacultyName
FROM Teachers AS t, Faculties AS f, GroupsLectures AS gl, Departments as d, Lectures AS l, Groups AS g
WHERE t.Id = l.TeacherId AND l.Id = gl.LectureId AND gl.GroupId = g.Id AND g.DepartmentId = d.Id AND d.FacultyId = f.Id

-- 6. Print names of the departments and names of the groups that relate to them. 
SELECT d.Name AS DepartmentName, g.Name AS GroupName
FROM Departments AS d, Groups AS g
WHERE g.DepartmentId = d.Id

-- 7. Print names of the subjects that the teacher "Amy Black" teaches.
SELECT s.Name
FROM Subjects AS s, Teachers AS t, Lectures AS l
WHERE s.Id = l.SubjectId AND l.TeacherId = t.Id AND t.Name = 'Amy' AND t.Surname = 'Black'

-- 8. Print names of the departments, where "Network Security" is taught.
SELECT d.Name
FROM Departments AS d, Groups AS g, GroupsLectures AS gl, Lectures AS l, Subjects AS s
WHERE d.Id = g.DepartmentId AND g.Id = gl.GroupId AND gl.LectureId = l.Id AND l.SubjectId = s.Id AND s.Name = 'Network Security'

--9. Print names of the groups that belong to the "Computer Science" faculty. 
SELECT g.Name
FROM Groups AS g, Departments AS d, Faculties AS f
WHERE g.DepartmentId = d.Id AND d.FacultyId = f.Id AND f.Name = 'Computer Science'

-- 10. Print names of the 5th year groups, as well as names of the faculties to which they relate.
SELECT g.Name AS GroupName, f.Name AS FacultyName
FROM Groups AS g, Faculties AS f, Departments AS d
WHERE g.DepartmentId = d.Id AND g.Year = 5 AND d.FacultyId = f.Id

-- 11. Print full names of the teachers and lectures they deliver (names of subjects and groups), and select only those lectures that are delivered in the classroom "A312".
SELECT t.Name + ' ' + t.Surname AS TeacherFullName, s.Name AS SubjectName, g.Name AS GroupName
FROM Teachers AS t, Lectures AS l, Subjects AS s, Groups AS g, GroupsLectures AS gl
WHERE t.Id = l.TeacherId AND l.SubjectId = s.Id AND l.Id = gl.LectureId AND gl.GroupId = g.Id AND l.LectureRoom = 'A312'