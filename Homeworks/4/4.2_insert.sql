INSERT INTO Faculties (Name, Financing)
VALUES ('Economics', 62000),
('Computer Science', 75000),
('Arts', 58000);

INSERT INTO Departments(Name, Financing, FacultyId)
VALUES ('Finance', 32000, 1),
('Cybersecurity', 40000, 2),
('Linguistics', 29000, 3);

INSERT INTO Groups(Name, Year, DepartmentId)
VALUES ('E120', 3, 1),
('C108', 5, 2),
('A241', 4, 3);

INSERT INTO Curators(Name, Surname)
VALUES ('Anna', 'Smith'),
('David', 'Jones'),
('Dan','Johnson');

INSERT INTO GroupsCurators(CuratorId, GroupId)
VALUES (1,3),
(2,2),
(3,1);

INSERT INTO Teachers(Name, Surname, Salary)
VALUES ('Amy', 'Black', 1050),
('Arthur', 'Wiliams', 950),
('Jamie', 'Brown', 1200);

INSERT INTO Subjects(Name)
VALUES ('Financial markets'),
('Investments'),
('Network Security'),
('English literature');

INSERT INTO Lectures(LectureRoom, SubjectId, TeacherId)
VALUES ('A312', 1, 2),
('P102', 2, 2),
('A301', 3, 3),
('P230', 4, 1);

INSERT INTO GroupsLectures (GroupId, LectureId)
VALUES (1, 1),
(1, 2),
(2, 3),
(3, 4);