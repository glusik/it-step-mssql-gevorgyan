INSERT INTO Faculties (Dean, Name)
VALUES ('Adam Smith', 'Economics'),
('George Brown', 'Math'),
('Lindsey Jones', 'Linguistics'),
('Amy McCullie', 'Social Studies');

INSERT INTO Departments (Name, Financing)
VALUES ('Finance', 35000),
('Statistics', 40000),
('English language', 12000),
('Political Science', 23000);

INSERT INTO Teachers (EmploymentDate, IsAssistant, IsProfessor, Name, Surname, Position, WageRate, Premium)
VALUES ('2010-04-30', 0, 1, 'Kate', 'Sullivan', 'Leturer', '1400', '300'),
('2016-08-01', 1, 0, 'Albert', 'Odom', 'Assistant', '1100', '200'),
('2012-07-24', 0, 1, 'Jacob', 'Joy', 'Lecturer', '1800', '400'),
('2015-02-20', 1, 0, 'Annie', 'Oliver', 'Assistant', '1100', '200'),
('2017-03-14', 1, 0, 'Joshua', 'Terry', 'Assistant', '850', '250');

INSERT INTO Groups (Name, Rating, Year)
VALUES ('E1', 3, 2),
('E2', 5, 3),
('M1', 5, 4),
('S1', 4, 2),
('L1', 3, 5);